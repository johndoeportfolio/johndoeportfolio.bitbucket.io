# Software Development Skills: Front-End
## Nestori Kangashaka
#### You can access the website [from here](https://johndoeportfolio.bitbucket.io/project/dist/index.html).
#### The learning diary can be found in the LearningDiary.txt file.
#### The coursework is in the 'coursework' folder and the project is in the 'project' folder.
#### Video demonstration [here](https://lut-my.sharepoint.com/:v:/g/personal/nestori_kangashaka_student_lut_fi/Edv9RgS8VuNDi8pjPGuAvkYBEe_Ep03qCAu8MMh4DzD7-g?e=DE3TCG).